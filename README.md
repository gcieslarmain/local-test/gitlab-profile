# Guide to Automated Library Publishing with GitLab CI/CD

## Overview

This is an example of how to automatically `build`, `version`, and `deploy` (publish) __libraries/packages__ to a GitLab `npm registry` (public or private) using __CI/CD pipelines__.

Picture this: you're working on multiple applications (Angular and Angular/Ionic apps in this example), and you recognize the need to reuse certain services, components, pipes, and other code snippets across your projects. This guide demonstrates the complete procedure, from crafting these libraries to publishing them in your private npm registry on GitLab, and subsequently importing and utilizing them in your applications.

Furthermore, we'll tackle __automated versioning__ using semantic commit messages. To achieve this, we'll leverage the [semantic-release](https://github.com/semantic-release/semantic-release) tool. It's responsible for analyzing commit messages, orchestrating releases, curating a changelog, publishing the packages, and syncing the changes back to your repository. This entire process is seamlessly triggered after committing and pushing your updates.

## Step by step

These will be our steps:

1. [Create the consumer apps](#1-create-the-consumer-apps)
2. [Create the library workspace](#2-create-the-library-workspace)
3. [Push to GitLab](#3-push-to-gitlab)
4. [CI/CD setup](#4-cicd-setup)
5. [Registry setup](#5-registry-setup)
6. [Release + versioning setup](#6-release-versioning-setup)
7. [Release packages](#7-release-packages)
8. [Consume packages](#8-consume-packages)


# 1. Create the consumer apps

We will create two basic apps for consuming (installing and using) our future npm packages. 

- app-one: an Angular application
- app-two: an Angular/Ionic application

## Create app-one:

Run:

    $ npm install -g @angular/cli
    $ ng new app-one --style=css --routing=false

## Create app-two:

I've set up an Angular/Ionic application for this repository. However, if you prefer, you can create an Angular app by running again the last command and changing the name to `app-two`.
For the Ionic app, execute:

    $ npm install -g @ionic/cli
    $ ionic start your-app-name tabs --type=angular --no-interactive

# 2. Create the library workspace

## Create the workspace

Now, we will create the Angular workspace `my-libraries` that will hold the libraries that we will publish as npm packages, and later import and use in our sample apps. Run:

    $ ng new my-libraries --create-application=false

## Generate the libraries

 For creating two sample libraries, run:

     cd my-libraries
     ng generate library library-one
     ng generate library library-two

We have now two libraries that we want to publish into our own npm registry. In this example the projects and the registry is public, but you can make everything private.

# 3. Push to GitLab

In your GitLab repository, create a group (optional) and four projects inside:

- app-one
- app-two
- my-libraries
- npm-registry (or whatever name you want)

Upload your local project to the relevant GitLab repositories by following the instructions provided on the homepage of each GitLab project.

# 4. CI/CD setup

In the root of the my-libraries project, create the file `.gitlab-ci.yml`. This file contains the instructions for the pipeline to run every time you push a relevant change to the project. The pipeline has two main tasks: building and releasing the package.

Let's check the first part of the file:

```
image: node:alpine

stages:
  - build
  - release

before_script:
  - npm ci

.template-build:
  stage: build
  script:
    - npm install -g @angular/cli
    - ng build $LIB_NAME
  artifacts:
    paths:
      - dist/$LIB_NAME/
  only:
    refs:
      - main
  resource_group: library-build

.template-release:
  stage: release
  script:
    - apk add --no-cache git
    - cp .npmrc ./dist/$LIB_NAME
    - cd dist/$LIB_NAME
    - npx semantic-release
  only:
    refs:
      - main
  resource_group: library-release

```
- I'm using a `node:alpine` docker image, since it is very compact.
- Two stages are defined, that will run one after the other: `build` and `release`.
- Before each job runs I want to be sure that all our dependencies are installed, with `npm ci`.
- There are two job `templates` defined: the first for the build stage and the second for the release stage. I'm using templates because I need to create two jobs for every library and I don't want to repeat the same code over and over again.
- The variable `$LIB_NAME` will be defined in the jobs that will extend these templates.
- In the build template I install Angular and `build` the library, configure the `dist folder` as an artifact to share with the following job, and restrict the job to run only when changes are commited to the `main` branch.
- The release template will install git, copy the .npmrc file (we will see it later) to the dist folder and executing the [semantic-release](https://www.npmjs.com/package/semantic-release) tool, that will be on charge of all the releasing and versioning duties.

The second part of the file:

```
# Library one
build-library-one:
  extends:
    - .template-build
  variables:
    LIB_NAME: library-one
  only:
    changes:
      - projects/library-one/**/*

release-library-one:
  extends:
    - .template-release
  variables:
    LIB_NAME: library-one
  needs:
    - build-library-one
  only:
    changes:
      - projects/library-one/**/*

```

These are the actual jobs for the library-one library. By __extending the templates__, they add to them some additional specific instructions. They define the `variable` that will be used in the template (the name of the library) and restricts the execution to the changes uploaded to a specific projects subfolder.

This structure has to be repeated for every library we have in the workspace (see the [file](https://gitlab.com/gcieslarmain/frontend-libraries-ci/my-libaries/-/blob/82fcbcf6d96b01a8e181a1642566dbceff32e94f/.gitlab-ci.yml)).


# 5. Registry setup

In this step, we will:

- Define the location of our npm registry
- Configure the library workspace for it to be able to publish packages
- Configure the GitLab environment to allow our pipeline to interact with the registry

## Configure the npm registry

This it's gonna be very straightforward: any project is capable of holding a package registry. In our case, we will use the empty `npm-registry` project we created before. Navigate to [that project](https://gitlab.com/gcieslarmain/frontend-libraries-ci/npm-registry) and write down its `Project ID`. 

To see the packages in the registry, select the __Package Registry__ option from the __Deploy__ section in the left sidebar.

Now you must configure your libraries workspace for it to know the registry ID where it must upload the packages to. For that purpose, in the root of the `my-libraries` project, create a [.npmrc](https://gitlab.com/gcieslarmain/frontend-libraries-ci/my-libaries/-/blob/82fcbcf6d96b01a8e181a1642566dbceff32e94f/.npmrc) file with the contents:

    @<scope>:registry=https://gitlab.com/api/v4/projects/<project-id>/packages/npm/
    //gitlab.com/api/v4/projects/<project-id>/packages/npm/:_authToken=${NPM_TOKEN}

I used the name of my root group for the scope, and replaced the project id:
 
    @gcieslarmain:registry=https://gitlab.com/api/v4/projects/48607800/packages/npm/
    //gitlab.com/api/v4/projects/48607800/packages/npm/:_authToken=${NPM_TOKEN}

__Let's break down the first line:__

_@gcieslarmain_: This is a [scope](https://docs.npmjs.com/cli/v6/using-npm/scope) in npm. Scopes are a way of grouping related packages together, and they also ensure a unique namespace for your packages, which can help prevent naming collisions with public packages. When you see a package named like `@gcieslarmain/package-name`, it means the package package-name is under the @gcieslarmain scope.

_:registry_: This is an npm configuration key that specifies the URL of the registry. When you combine it with a scope at the beginning (as in @gcieslarmain:registry), it means this registry URL will only be used for packages that are under the specified scope.

_https://gitlab.com/api/v4/projects/48607800/packages/npm/_: This URL points to the npm registry provided by GitLab for a specific project. So, when you try to install or publish a package with the @gcieslarmain scope, npm will know to interface with this specific GitLab project's npm registry.

__The second line:__

This line is setting authentication for the specified GitLab npm 
registry. Whenever npm interacts with this registry (like when publishing a package), it will use the authentication token specified by the `${NPM_TOKEN}` environment variable to authenticate the request.
Essentially, this says: "When accessing this GitLab registry, authenticate using the token provided in the NPM_TOKEN environment variable."

We will set the environment variable later.

## Create access and deploy tokens

A couple of tokens will be necessary:

- __Deploy token__: to authenticate the packege publishing to the registry. Navigate to the `npm-registry` project > Settings > Repository > Deploy tokens > Add token, and create a token named NPM_TOKEN, without expiration date, without username, and select all the scopes. Copy the token in a safe place.

- __Personal access token__: to authenticate the git interaction with the repository. Click in your profile picture > Preferences > Access tokens > Add new token > enter any name, expiration date (max 1 year), and select all scopes. Copy the token in a safe place.

## Create CI/CD variables

We need to save the tokens in variables, for the pipelines and processes to be able te read them when they execute. Never hardcode the tokens in your files.

Navigate to my-libaries project > Settings > CI/CD > Variables > Add variables:

- `NPM_TOKEN`: deploy token value
- `GITLAB_TOKEN`: personal access token value
- `GH_TOKEN`: personal access token value
- `GITHUB_TOKEN`: personal access token value

# 6. Release + versioning setup

## Overview

For publishing the package to the npm registry, we could simply run a command like `npm publish` in the pipeline. However, I chose to use the [semantic-release](https://github.com/semantic-release/semantic-release) tool because:

> ...it automates the whole package release workflow including: determining the next version number, generating the release notes, and publishing the package.

> semantic-release uses the __commit messages__ to determine the consumer impact of changes in the codebase. Following formalized conventions for commit messages, semantic-release automatically determines the next semantic version number, generates a changelog and publishes the release.

>By default, semantic-release uses [Angular Commit Message Conventions](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-format).

## Install semantic-release and plugins

In the `my-libraries` project, install semantic-release and the plugins that will be needed:

    npm install --save-dev semantic-release @semantic-release/changelog @semantic-release/git @semantic-release/gitlab

See the [plugins docs](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/plugins.md).

## Create config file

In the root of the `my-libaries` project create the configuration file por the semantic-release. I choosed to use a js module (release.config.js) because it gives me more flexibility for replacing variables, but you can also use a YAML or JSON format. See the [configuration docs](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/configuration.md).

```
module.exports = {
  "branches": ["main"],
  "repositoryUrl": "https://gitlab.com/gcieslarmain/frontend-libraries-ci/my-libaries.git",
  "tagFormat": "${process.env.LIB_NAME}-v${version}",
  "plugins": [
    "@semantic-release/commit-analyzer",
    "@semantic-release/release-notes-generator",
    ["@semantic-release/changelog", {
      "changelogFile": "CHANGELOG.md"
    }],
    ["@semantic-release/npm", {
      "npmPublish": true,
      "tarballDir": "dist",
      "registry": "https://gitlab.com/api/v4/projects/48607800/packages/npm/"
    }],
    [
      "@semantic-release/git",
      {
        "assets": ["package.json", "CHANGELOG.md"],
        "message": "chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}\n\n${process.env.LIB_NAME} git plugin"
      }
    ],
    ["@semantic-release/gitlab",
      {
        "assets": ["package.json", "CHANGELOG.md"],
        "message": "chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}\n\n${process.env.LIB_NAME} gitlab plugin"
      }
    ]
  ]
}
```
Here's a breakdown of what each part of this configuration does:

1. **`branches`**: 
   - Specifies that releases should only occur from the `main` branch.

2. **`repositoryUrl`**: 
   - The URL of the repository on GitLab.

3. **`tagFormat`**: 
   - Format for the git tags. Uses `LIB_NAME` environment variable as a prefix followed by `-v`. This is for having differentiating tags among libraries.

4. **`plugins`**: List of plugins to customize the release process:

   - **`@semantic-release/commit-analyzer`**: 
     - Analyzes commit messages for determining version bumps.

   - **`@semantic-release/release-notes-generator`**: 
     - Generates release notes from commit messages.
   
   - **`@semantic-release/changelog`**: 
     - Handles the `CHANGELOG.md` file.
   
   - **`@semantic-release/npm`**: 
     - Updates `package.json` and publishes to npm.
       - `npmPublish`: Publishes package to npm.
       - `tarballDir`: Directory for the tarball.
       - `registry`: URL for the npm registry on GitLab.
     
   - **`@semantic-release/git`**: 
     - Creates a release commit in the repository.
       - `assets`: Files (`package.json`, `CHANGELOG.md`) in the release commit.
       - `message`: Commit message format, includes release version, notes, and the `LIB_NAME` value with "git plugin".
     
   - **`@semantic-release/gitlab`**:  
     - Manages releases on GitLab's UI.


# 7. Release packages

Now that we have our setup ready, publishing a package is extremely easy. We just need to write a commit message. That's it!

By using the [Angular Commit Message Conventions](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-format) to write the commit messages, semantic-release can decide how to increment the version number for the following package release and manage the entire release process. For exemple, I modify the template text in the `LibraryOneComponent` and make the commit:

    git add .
    git commit -m "fix(component): improve display text
    git push

This is what will happend in the repository side:

1. The pipeline will be triggered, because we are pushing to the main branch.
2. Only the library-one's jobs will be executed, since we made changes only in its folder.
3. The build stage will be executed, and the release stage will wait for it to complete.
4. The release stage will run and execute semantic-release, following the [release seteps](https://github.com/semantic-release/semantic-release#release-steps).

As a result:

- A new package will be available in the [package registry](https://gitlab.com/gcieslarmain/frontend-libraries-ci/npm-registry/-/packages).
- A new release will be generated and visible in the [releases page](https://gitlab.com/gcieslarmain/frontend-libraries-ci/my-libaries/-/releases).
- The [CHANGELOG.md](https://gitlab.com/gcieslarmain/frontend-libraries-ci/my-libaries/-/blob/82fcbcf6d96b01a8e181a1642566dbceff32e94f/dist/library-one/CHANGELOG.md) will be updated.
- A new tag (snapshot) will be added to the my-library project.

# 8. Consume packages

In order to install our packages in the applications, we must add a `.npmrc` file in the project's root folder to contain the configuration. 

It will be exactly the same file as the one we added in the _my-libraries_ project, but this time, it will be used for reading purposes instead of writing/publishing. 

    @gcieslarmain:registry=https://gitlab.com/api/v4/projects/48607800/packages/npm/
    //gitlab.com/api/v4/projects/48607800/packages/npm/:_authToken=${NPM_TOKEN}

Replace "gcieslarmain" with your `scope` and replace the `project ID` with yours (the project ID of the npm-registry that holds the registry).

Notice that the configuration is expecting an environment variable `NPM_TOKEN` that will contain the `deploy token` that authorizes the reading access to the registry.

For setting the environment variable in Windows, use one of the following commands in the _command prompt_, according to the persistence of the variable you desire in your environment. You may need to restart your prompt if you use the setx command (permanent persistence of the variable).

    setx NPM_TOKEN <deploy token>
    set NPM_TOKEN=<deploy token>

You can replace the `deploy token` with the token you used previously or create a second token with read only scopes, if you need to give it to someone that shouldn't have publishing permissions.

And finally, run the following command for installing the package:

    npm install @<scope>/<package-name>@<version>

The package will be added to your project’s dependencies in the `package.json` file and downloaded to node_modules folder.



